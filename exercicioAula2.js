let alunos = [
      {
        "nome": "Pedro",
        "turma": "A",
        "nota1": 10,
        "nota2": 7
    },
    {
        "nome": "Maria",
        "turma": "B",
        "nota1": 7,
        "nota2": 4
    },
    {
        "nome": "Jonathan",
        "turma": "C",
        "nota1": 9,
        "nota2": 9
    },
    {
        "nome": "Gabigol",
        "turma": "C",
        "nota1": 10,
        "nota2": 9
    },
  {
        "nome": "Everton Ribeiro",
        "turma": "A",
        "nota1": 9,
        "nota2": 10
    },
  {
        "nome": "Bruno Henrique",
        "turma": "B",
        "nota1": 0,
        "nota2": 5
    },
];

const DefinirMedia = (array) => {
  let maiorMediaA = {};   let mediaA = 0;  let mediaTempA = 0; 
  let maiorMediaB = {};  let mediaB = 0;  let mediaTempB = 0;
  let maiorMediaC = {};  let mediaC = 0;  let mediaTempC = 0;


  for(i in array) {
    if(array[i].turma === "A") {
      mediaA = (array[i].nota1 + array[i].nota2) / 2.0;
      if(mediaA > mediaTempA) {
        mediaTempA = mediaA;
        maiorMediaA = array[i];
      }
    }
    else if(array[i].turma === "B") {
      mediaB = (array[i].nota1 + array[i].nota2) / 2.0;
      if(mediaB > mediaTempB) {
        mediaTempB = mediaB;
        maiorMediaB = array[i];
      }
    }
    else if(array[i].turma === "C") {
      mediaC = (array[i].nota1 + array[i].nota2) / 2.0;
      if(mediaC > mediaTempC) {
        mediaTempC = mediaC;
        maiorMediaC = array[i];
      }
    }
  }
  return `A melhor media da turma A foi do(a) ${maiorMediaA.nome} com ${mediaTempA}\nA melhor da turma B foi do(a) ${maiorMediaB.nome} com ${mediaTempB}\nA melhor media da turma C foi do(a) ${maiorMediaC.nome} com ${mediaTempC}`;
};

console.log(DefinirMedia(alunos));
